module.exports = {
    root: true,
    env: {
        node: true,
    },
    extends: [
        'plugin:vue/essential',
        '@vue/airbnb',
    ],
    parserOptions: {
        // parser: 'babel-eslint',
        parser: '@babel/eslint-parser',
    },
    rules: {
        // 'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
        'no-console': 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',

        indent: 'off',
        // 'vue/html-indent': ['error', 4],
        'vue/script-indent': ['error', 4, { baseIndent: 0 }],

        'no-multiple-empty-lines': 0,

        // 'max-len': ['error', {
        //     code: 120
        // }],
        'max-len': 0,

        semi: [
            2,
            'always',
        ],

        'space-before-function-paren': [
            'error',
            'never',
        ],

        'padded-blocks': 0,

        'operator-linebreak': [
            'error',
            'after',
        ],

        'object-curly-newline': ['error', {
            'ImportDeclaration': 'never',
        }],

        // 'no-param-reassign': [
        //     error,
        //     {
        //         props: true,
        //         ignorePropertyModificationsFor: [
        //             'state',
        //         ],
        //     },
        // ],
        'no-param-reassign': [
            'error',
            {
                'props': false,
            },
        ],

        'global-require': 0,
        "linebreak-style": 0,
    },
};
