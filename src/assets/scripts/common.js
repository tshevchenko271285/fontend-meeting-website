import Vue from 'vue';

// variables
Vue.prototype.$daysOfWeek = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];


// add zeros at start
Vue.prototype.$addZeroesAtStart = function $addZeroesAtStart(num, digitsNum = 2) {
    if (Number.isNaN(num) ||
        num === null ||
        num === undefined) {
        return num;
    }
    const numStr = num.toString();
    const zeroesNum = digitsNum - numStr.length;
    if (zeroesNum <= 0) {
        return numStr;
    }
    return (new Array(zeroesNum + 1).join('0') + numStr);
};


// get date data by string
Vue.prototype.$getDateDataByString = function $getDateDataByString(fullDateStr) {
    // '2021-10-16T18:30:00+00:00'
    const fullDateArr = fullDateStr.split('T');
    const dateArr = fullDateArr[0].split('-');
    const timeArrFull = fullDateArr[1].split('+');
    const timeArr = timeArrFull[0].split(':');
    const year = parseInt(dateArr[0], 10);
    const month = parseInt(dateArr[1], 10) - 1;
    const day = parseInt(dateArr[2], 10);
    const date = new Date(year, month, day);
    const dayOfWeek = this.$daysOfWeek[date.getDay()];
    return {
        date: `${Vue.prototype.$addZeroesAtStart(day)}.${Vue.prototype.$addZeroesAtStart(month + 1)}.${year}`,
        dayOfWeek,
        time: `${timeArr[0]}:${timeArr[1]}`,
    };
};


// uppercase first letter
Vue.prototype.$uppercaseFirstLetter = function $uppercaseFirstLetter(str) {
    return str[0].toUpperCase() + str.substring(1);
};


// scroll to elem
Vue.prototype.$scrollToElem = function $scrollToElem(selector) {
    const elem = document.querySelector(selector);
    if (!elem) {
        return;
    }

    const elemRect = elem.getBoundingClientRect();
    window.scrollTo({
        left: 0,
        top: elemRect.top,
        behavior: 'smooth',
    });
};

Vue.prototype.$hasError = function hasError(field) {
    return field.$dirty && field.$invalid;
};

Vue.prototype.$getErrorMessage = function getErrorMessage(field) {
    let msg = '';
    if (!field.$errors.length) return '';
    const error = field.$errors[0];
    const { type } = error.$params;
    switch (type) {
        case 'minLength': msg = `Это поле должно быть не менее ${error.$params.min} символов`; break;
        case 'required': msg = 'Это обязательное поле'; break;
        case 'email': msg = 'Поле должно быть валидным E-mail'; break;
        case 'sameAs': msg = 'Пароли не совпадают'; break;
        default: msg = ''; break;
    }

    return msg;
};

Vue.prototype.$chunkArray = (arr, size) => {
    let counter = 0;
    const chunkArray = [];
    while (arr.length > counter) {
        const chunk = [];
        for (let i = 0; i < size; i += 1) {
            if (arr[counter]) {
                chunk.push(arr[counter]);
            }
            counter += 1;
        }
        chunkArray.push(chunk);
    }

    return chunkArray;
};

// declension([ 'день', 'дня', 'дней' ], days);
Vue.prototype.$declension = (forms, val) => {
    const cases = [2, 0, 1, 1, 1, 2];
    return forms[(val % 100 > 4 && val % 100 < 20) ? 2 : cases[(val % 10 < 5) ? val % 10 : 5]];
};

Vue.prototype.$calculateAge = (date) => {
    const fullYears = (-1970) + new Date((new Date() - new Date(date))).getFullYear();
    return Number.isNaN(fullYears) ? '' : fullYears;
};
