import cssVars from 'css-vars-ponyfill';
import '@/assets/scripts/common';
// import '@/assets/styles/base.scss';
import axios from 'axios';

import Vue from 'vue';
import App from './App.vue';
import mixinsGlobal from './mixins/global';
import vuetify from './plugins/vuetify';
import router from './router';
import store from './store';

import '@/assets/styles/base.scss';


cssVars();

Vue.config.productionTip = false;
Vue.mixin(mixinsGlobal);

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8883',
});
Vue.prototype.$http = axiosInstance;

new Vue({
    // mixins: [mixinsGlobal],
    router,
    store,
    vuetify,
    render: (h) => h(App),
}).$mount('#app');
