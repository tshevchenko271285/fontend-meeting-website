import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#2b396d',
                secondary: '#4f5778',
                error: '#dc0000',
            },
        },
        options: {
            customProperties: true,
        },
    },
    breakpoint: {
        thresholds: {
            xs: 757,
            sm: 1013,
            md: 1280,
            lg: 1800,
        },
        scrollBarWidth: 17,
    },
});
