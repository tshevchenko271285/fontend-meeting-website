export default class ProfileModel {

    constructor(params = {}) {
        this.name = params.name ? params.name : '';
        this.dateOfBirth = params.dateOfBirth ? params.dateOfBirth : '';
        this.country = params.country ? params.country : '';
        this.city = params.city ? params.city : '';
        this.aboutMe = params.aboutMe ? params.aboutMe : '';
        this.purposeOfDating = params.purposeOfDating ? params.purposeOfDating : '';
        this.photoPrice = params.photoPrice ? params.photoPrice : 0;
        this.videoPrice = params.videoPrice ? params.videoPrice : 0;
        this.videoChatDuration = params.videoChatDuration ? params.videoChatDuration : 0;
        this.videoChatPrice = params.videoChatPrice ? params.videoChatPrice : 0;
        this.oneHourOut = params.oneHourOut ? params.oneHourOut : 0;
        this.oneHourAtHome = params.oneHourAtHome ? params.oneHourAtHome : 0;
        this.twoHourOut = params.twoHourOut ? params.twoHourOut : 0;
        this.twoHourAtHome = params.twoHourAtHome ? params.twoHourAtHome : 0;
        this.nightOut = params.nightOut ? params.nightOut : 0;
        this.nightAtHome = params.nightAtHome ? params.nightAtHome : 0;
        this.sexualPreferences = params.sexualPreferences ? params.sexualPreferences : '';
        this.weekPrice = params.weekPrice ? params.weekPrice : 0;
        this.monthPrice = params.monthPrice ? params.monthPrice : 0;
        this.relationshipFormat = params.relationshipFormat ? params.relationshipFormat : [];
        this.interests = params.interests ? params.interests : [];
        this.height = params.height ? params.height : 0;
        this.weight = params.weight ? params.weight : 0;
        this.breastSize = params.breastSize ? params.breastSize : '';
        this.heirColor = params.heirColor ? params.heirColor : '';
        this.eyeColor = params.eyeColor ? params.eyeColor : '';
        this.children = params.children ? params.children : '';
        this.tatoo = params.tatoo ? params.tatoo : '';
        this.smoking = params.smoking ? params.smoking : '';
        this.other = params.other ? params.other : '';
        this.communicationFormat = params.communicationFormat ? params.communicationFormat : '';
        this.mainPhoto = params.mainPhoto ? params.mainPhoto : 0;
        this.photos = params.photos ? params.photos : [];
        this.email = params.email ? params.email : '';
        this.password = params.password ? params.password : '';
        this.passwordConfirm = params.passwordConfirm ? params.passwordConfirm : '';
        this.vip = params.vip ? params.vip : false;
        this.approved = params.approved ? params.approved : false;
        this.likesCount = params.likesCount ? params.likesCount : 0;
        this.isLike = params.isLike ? params.isLike : false;
        this.isWink = params.isWink ? params.isWink : false;
        this.isMessaged = params.isMessaged ? params.isMessaged : false;
    }

    get avatar() {
        if (this.photos.length) {
            const mainPhoto = this.photos.find((photo) => photo.id === this.mainPhoto);
            return mainPhoto;
        }
        return {
            src: '',
        };
    }

    get age() {
        const fullYears = (-1970) + new Date((new Date() - new Date(this.dateOfBirth))).getFullYear();
        return Number.isNaN(fullYears) ? '' : fullYears;
    }
}
