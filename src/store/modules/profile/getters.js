// getters
export default {
    Profile: (state) => state.profileData,
    breastSizes: (state) => state.breastSizes,
    heirColors: (state) => state.heirColors,
    eyeColors: (state) => state.eyeColors,
    interestsList: (state) => state.interestsList,
    relationshipFormatList: (state) => state.relationshipFormatList,
    getMainPhoto: (state) => state.profileData.photos.find((photo) => photo.id === state.profileData.mainPhoto),
}; // getters
