// actions
// import axios from 'axios';

export default {
    saveProfile({ commit }, { profile }) {
        commit('setProfileData', profile);
    },
}; // actions
