// mutations
export default {
    setEyes(state, eyes) {
        state.eyes = eyes;
    },

    setHairs(state, hairs) {
        state.hairs = hairs;
    },

    setSexes(state, sexes) {
        state.sexes = sexes;
    },

    setCommunicationTypes(state, communicationTypes) {
        state.communicationTypes = communicationTypes;
    },

    setSmokings(state, smokings) {
        state.smokings = smokings;
    },

    setCountries(state, countries) {
        state.countries = countries;
    },
}; // mutations
