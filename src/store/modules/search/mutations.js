// mutations
export default {
    setResult(state, { searchResult }) {
        state.result = searchResult;
    },
    setParams(state, { params }) {
        state.params = params;
    },
    setCurrentPage(state, { currentPage }) {
        state.currentPage = currentPage;
    },
    setCountPages(state, { countPages }) {
        state.countPages = countPages;
    },
    setVipProfiles(state, { vipProfiles }) {
        state.vipProfiles = vipProfiles;
    },
}; // mutations
