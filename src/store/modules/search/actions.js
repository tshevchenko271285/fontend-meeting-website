// actions
// import axios from 'axios';s
import ProfileModel from '@/store/modules/profile/ProfileModel';

const cities = ['Одесса', 'Киев', 'Запорожье', 'Харьков', 'Сумы', 'Николаев', 'Херсон'];
const names = ['Антон', 'Максим', 'Николай', 'Андрей', 'Сергей', 'Влад', 'Марк'];
const dateOfBirthdays = ['1999-04-08', '1998-05-07', '1997-06-06', '2000-07-05', '2001-08-04', '2002-09-03', '2003-10-02'];
const targets = ['online', 'keep-woman', 'relationship', 'escort'];

export default {
    search({ commit }) {
        const searchResult = [];
        for (let i = 0; i < 6; i += 1) {
            const profile = new ProfileModel({
                country: 'Украина',
                city: cities[Math.floor(Math.random() * cities.length)],
                name: names[Math.floor(Math.random() * names.length)],
                dateOfBirth: dateOfBirthdays[Math.floor(Math.random() * dateOfBirthdays.length)],
                purposeOfDating: targets[Math.floor(Math.random() * targets.length)],
                mainPhoto: 1,
                photos: [
                    { id: 1, src: `/images/search/temp/search-result-item-${Math.floor(Math.random() * 6)}.png`, approved: true },
                    { id: 2, src: `/images/search/temp/search-result-item-${Math.floor(Math.random() * 6)}.png`, approved: false },
                ],
                vip: Math.random() < 0.5,
                approved: Math.random() < 0.5,
            });
            if (Math.random() < 0.5) {
                profile.photos.push({ id: 3, src: `/images/search/temp/search-result-item-${Math.floor(Math.random() * 6)}.png`, approved: false });
            }
            searchResult.push(profile);
        }

        commit('setResult', { searchResult });
    },

    loadVipProfiles({ commit }) {
        const vipProfiles = [];
        for (let i = 0; i < 7; i += 1) {
            vipProfiles.push(new ProfileModel({
                country: 'Украина',
                city: cities[Math.floor(Math.random() * cities.length)],
                name: names[Math.floor(Math.random() * names.length)],
                dateOfBirth: dateOfBirthdays[Math.floor(Math.random() * dateOfBirthdays.length)],
                mainPhoto: 1,
                photos: [{ id: 1, src: `/images/search/temp/search-vip-${i + 1}.jpg`, approved: true }],
            }));
        }

        commit('setVipProfiles', { vipProfiles });
    },
}; // actions
