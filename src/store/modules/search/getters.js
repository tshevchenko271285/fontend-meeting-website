// getters
export default {
    result: (state) => state.result,
    params: (state) => state.params,
    currentPage: (state) => state.currentPage,
    countPages: (state) => state.countPages,
    vipProfiles: (state) => state.vipProfiles,
}; // getters
