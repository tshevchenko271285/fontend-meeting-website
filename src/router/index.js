import Vue from 'vue';
import VueRouter from 'vue-router';

import Registration from '@/views/registration/Registration.vue';
import Profile from '@/views/profile/Profile.vue';
import ProfileEdit from '@/views/profile-edit/ProfileEdit.vue';
import Search from '@/views/search/Search.vue';
import Faq from '@/views/faq/Faq.vue';


Vue.use(VueRouter);

const routes = [

    {
        path: '/registration',
        name: 'Registration',
        component: Registration,
    },
    {
        path: '/profile',
        name: 'Profile',
        component: Profile,
    },
    {
        path: '/profile-edit',
        name: 'ProfileEdit',
        component: ProfileEdit,
    },
    {
        path: '/search',
        name: 'Search',
        component: Search,
    },
    {
        path: '/faq',
        name: 'Faq',
        component: Faq,
    },
];

const router = new VueRouter({
    base: process.env.BASE_URL,
    routes,
});

export default router;
