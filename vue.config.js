module.exports = {
    transpileDependencies: [
        'vuetify',
        'strip-ansi',
    ],

    publicPath: '/',
    outputDir: '../../public/static',
    // outputDir: '../wwwroot',

    css: {
        loaderOptions: {
            scss: {
                prependData: '@import "~@/assets/styles/variables.scss";',
            },
        },
    },

    chainWebpack: (config) => {
        config.performance
            .maxEntrypointSize(2000000)
            .maxAssetSize(2000000);
    },

    productionSourceMap: false,

    lintOnSave: 'error',

    devServer: {
        // host: '0.0.0.0',
        host: 'localhost',
        port: 8084,

        // public: '0.0.0.0:8883',
        proxy: {

            '^/api': {
                target: 'http://localhost:80',
                changeOrigin: true,
                secure: false,
                // ws: true,
                logLevel: 'debug',
                pathRewrite: { '^/api': '/api' },
                // pathRewrite: {
                //     '^/api': '/api',
                // },
            },
        },

        // proxy: {
        //     '/api': {
        //         target: 'http://localhost:8883/api',
        //         changeOrigin: true,
        //         secure: false,
        //         logLevel: 'debug',
        //         pathRewrite:{
        //             '^/api': '',
        //         },
        //     }
        // },
    },
};
